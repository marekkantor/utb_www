import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

// Bootstrap components
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

function ErrorAlert(props: any) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Something went wrong!
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <>
          Wrong city name!
        </>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

function App() {
  const [ cityData, setCityData ] = useState<any>(null);
  const [ city, setCity ] = useState<string>("");
  const [ cityError, setCityError ] = useState<boolean>(false);

  const API_KEY = "40b91eb9cfb984e5a2e2dda11fc3b658";

  useEffect(() => {
    const c = localStorage.getItem('city');
    if (c) {
      loadCity(c);
    }
  }, []);

  const loadCity = (name: string) => {
    name = encodeURIComponent(name);
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${name}&appid=${API_KEY}`)
      .then((resp) => resp.json())
      .then((data) => {
        if (data.cod == "200") {
          setCityData(data);
          console.log(data);
        } else {
          setCityError(true);
        }
      }).catch((e) => {
        setCityError(true);
        setCityData(null);
      });
  }

  const onCityChange = (event: any) => {
    setCity(event.target.value);
  };

  const updateCity = () => {
    loadCity(city);
    localStorage.setItem('city', city);
  }

  const toCelsius = (k: number): number => {
    return k - 273.15;
  }

  let content: any = <div></div>;
  console.log(cityData);
  if (cityData) {
    content = <div>
      <h1>{cityData.name} | <small>{cityData.sys.country}</small></h1>
      <div>
        <b>Clouds</b> - {cityData.clouds.all}%
      </div>
      <div>
        <b>Temp</b> - {toCelsius(Number(cityData.main.temp)).toFixed(2)}°C
      </div>
      <div>
        <b>Max Temp</b> - {toCelsius(Number(cityData.main.temp_max)).toFixed(2)}°C
      </div>
      <div>
        <b>Min Temp</b> - {toCelsius(Number(cityData.main.temp_min)).toFixed(2)}°C
      </div>
      {/* { JSON.stringify(cityData) } */}
    </div>
  }

  return (
    <Container fluid={"sm"}>
      <Form noValidate>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>City</Form.Label>
          <Form.Control
            value={city}
            onChange={onCityChange}
            type="text"
            isInvalid={cityError}
          />
        </Form.Group>
      </Form>
      <Button onClick={updateCity} variant="primary">Search</Button>
      {content}
      <ErrorAlert
        show={cityError}
        onHide={() => setCityError(false)}
      />
    </Container>
  );
}

export default App;
